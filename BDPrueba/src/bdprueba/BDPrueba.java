/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bdprueba;

import java.util.Scanner;

/**
 *
 * @author curso
 */
public class BDPrueba {

    static final int REGISTRO = 1;
    static final int CONSULTA = 2;
    static final int GUARDAR = 3;
    static final int ELIMINAR = 4;
    static final int SALIR = 5;

    public static void main(String[] args) {
        System.out.println("Hola Mundo");
        BDComputadoras bd = new BDComputadoras();
        
        desplegarMenu();
        Scanner s = new Scanner(System.in);
        int opcion = 0;
        while ((opcion = s.nextInt()) != 5) {
            switch (opcion) {
                case REGISTRO:
                    bd.agregar(crearObjeto());
                    break;
                case CONSULTA:
                    System.out.println("Que objeto quieres imprimir (indice)?");
                    bd.desplegar(s.nextInt());
                    break;
                case GUARDAR:
                    //aqui se debria guardar en el archivo
                    System.out.println(bd.guardar());
                    break;
                case ELIMINAR:
                      System.out.println("Que objeto quieres eliminar (indice)?");
                    bd.eliminar(s.nextInt());
                    break;
            }
            desplegarMenu();
        }
    }

    public static void desplegarMenu() {
        System.out.println("Elija una opcion:");
        System.out.println("1 registro, 2 consulta, 3 GUARDAR, 4 eliminar 5 salir");
    }
    public static Computadora crearObjeto()
    {
        Computadora compu = new Computadora();
        Mouse m = new Mouse();
        m.setColor("negro");
        m.setMarca("marca mouse");
        compu.setMouse(m);
        System.out.println("Dame la marca");
        Scanner s = new Scanner(System.in);
        compu.setMarca(s.nextLine());
         System.out.println("Dame el numero de priocesadores");
        compu.setNumProcesadores(s.nextInt());
        return compu;
    }

}
