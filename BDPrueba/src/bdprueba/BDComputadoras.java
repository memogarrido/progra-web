/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bdprueba;

import com.google.gson.Gson;
import java.util.ArrayList;

/**
 *
 * @author curso
 */
public class BDComputadoras extends Archivo implements AdministradorArchivos{
    ArrayList<Computadora> computadoras;

    public BDComputadoras()
    {
        //aqyui deberia de leer del archivo  y convertir con gson
        computadoras= new ArrayList<>();
               
    }
    public ArrayList<Computadora> getComputadoras() {
        return computadoras;
    }

    public void setComputadoras(ArrayList<Computadora> computadoras) {
        this.computadoras = computadoras;
    }

    @Override
    public void eliminar(int pos) {
        computadoras.remove(pos);
    }

    @Override
    public void desplegar(int pos) {
        computadoras.get(pos).imprimir(pos);
    }

    @Override
    public void agregar(Object objeto) {
        computadoras.add((Computadora) objeto);
    }

    public String guardar ()
    {
        //aqui se debria guardar en el archivo en lugar de retornar
        Gson gson = new Gson();
        return gson.toJson(computadoras);
    }
    
    
}
