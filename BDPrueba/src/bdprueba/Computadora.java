/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bdprueba;

/**
 *
 * @author curso
 */
public class Computadora implements AdministradorObjeto{
    Mouse mouse;
    String marca;
    int numProcesadores;

    public Mouse getMouse() {
        return mouse;
    }

    public void setMouse(Mouse mouse) {
        this.mouse = mouse;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public int getNumProcesadores() {
        return numProcesadores;
    }

    public void setNumProcesadores(int numProcesadores) {
        this.numProcesadores = numProcesadores;
    }

    @Override
    public void imprimir(int pos) {
        System.out.println(pos + ": Marca: " + this.getMarca() + " Procesadores:" + this.getNumProcesadores());
    }
    
    
}
